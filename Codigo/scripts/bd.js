/*function onDeviceReady() {
console.log("entr�");
var db = window.openDatabase("bd_cnsc", "1.0", "Mis favoritas", 200000);
db.transaction(AgregarFavorito, errorOperacion, efectuadaOperacion);
}*/

function configurar_db() {

    function execute(tx) {
        //tx.executeSql('DROP TABLE IF EXISTS favoritos');
        tx.executeSql('CREATE TABLE IF NOT EXISTS favoritos (fecha_ingreso, id_convocatoria, nombre_convocatoria, oferta_empleo, fecha_fin, nro_dias)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS ultimasVistas (ultimaCantidad)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS TerminosCondiciones (Aprobados)');
        //tx.executeSql("UPDATE TerminosCondiciones SET Aprobados = 0");
        //tx.executeSql("INSERT INTO favoritos (fecha_ingreso, convocatoria, fecha_fin) "+
        //     "SELECT '2013-10-03', 'ABCconvocatoria', '2013-10-10' WHERE NOT EXISTS (SELECT * FROM favoritos WHERE convocatoria = 'ABCconvocatoria')");
    }

    function error(error) {
        console.log("Error al configurar base de datos", error)
    }

    function exito() {
        console.log("Configuraci�n exitosa")
    }

    var db = window.openDatabase("bd_cnsc", "1.0", "Mis favoritas", 200000);
    db.transaction(execute, error, exito);

}
//Scripts usados en la nueva plantilla

function consultarUltima() {
    var db = window.openDatabase("bd_cnsc", "1.0", "Consultar ultima", 200000);
    db.transaction(ConsultarUltimaCantidad, errorOperacionConsultaUltima, efectuadaOperacion);
}

function ConsultarUltimaCantidad(tx) {
    tx.executeSql('SELECT * FROM ultimasVistas', [], crearVariableUltimas, function (error) {
        console.log("consulta fechas fin error: " + error)
    });
}

function crearVariableUltimas(tx, results) {
    var len = results.rows.length;
    localStorage.setItem("cantidad_ultima", 999);
    for (var i = 0; i < len; i++) {
        localStorage.setItem("cantidad_ultima", results.rows.item(i).ultimaCantidad);
    }
    cargar_listaConvocatorias();
}

// Transaction error callback
//
function errorOperacionConsultaUltima(err) {
    console.log(err);
    alert("Error procesando la consulta de la ultima cantidad: " + err);
}
// Transaction success callback
//
function efectuadaOperacion() {
    console.log("Informaci�n Almacenada!");
}

function guardarUltimaCantidad() {
    var db = window.openDatabase("bd_cnsc", "1.0", "Guardar ultima", 100000);
    db.transaction(GuardarUltimoIngreso, errorOperacionGuardandoUltima, efectuadaOperacion);
}

function GuardarUltimoIngreso(tx) {
    var cantidad = localStorage.getItem("cantidad_ultima");
    tx.executeSql('DELETE FROM ultimasVistas');
    console.log("borr�");
    //    tx.executeSql('INSERT INTO ultimasVistas (ultimaCantidad) VALUES ("4")');
    tx.executeSql('INSERT INTO ultimasVistas (ultimaCantidad) VALUES ("' + cantidad + '")');
}

// Transaction error callback
//
function errorOperacionGuardandoUltima(err) {
    console.log(err);
    alert("Error procesando el almacenamiento de la ultima cantidad: " + err);
}

//Consultar bd recordatorios
function consultarBdRecordatorios() {
    var db = window.openDatabase("bd_cnsc", "1.0", "Consultar Recordatorio", 100000);
    db.transaction(consultarRecordatorios, errorOperacionGuardandoUltima, efectuadaOperacion);
}


//Funci�n para consultar las convocatorias pr�ximas a vencerse    
function consultarRecordatorios(tx) {
    tx.executeSql('SELECT * FROM favoritos', [], procesarColoresAlertas, function (error) {
        console.log("Error en la consulta de favoritos para definir alertas: " + error)
    });
}


//Permite definir seg�n la cantidad de d�as restantes a terminar la convocatoria
//el color correspondiente: 7-5 d�as azul, 3 d�as amarillo, 1 d�a rojo
function procesarColoresAlertas(tx, results) {
    var len = results.rows.length;
    var actual = obtenerFechaActual();
    var nroDias;
    var diasConfUsuario;
    var contador = 0;
    var textoFechas = "";
    localStorage.setItem("textoAlertas", "<div class='small-notification green-notification'><p class='parrafo-alertas'>No existen ofertas de empleo favoritas pr&oacute;ximas a cerrar</p></div> </div>");
    if (results.rows.length > 0) {
        localStorage.setItem("textoAlertas", "");
        for (var i = 0; i < len; i++) {

            nroDias = getNumeroDeNits(actual, results.rows.item(i).fecha_fin);
            diasConfUsuario = results.rows.item(i).nro_dias;

            if (nroDias <= diasConfUsuario) {
                if (nroDias <= 7 && nroDias >= 5) { contador++; textoFechas += "<div class='small-notification blue-notification'><p class='parrafo-alertas'>" + results.rows.item(i).nombre_convocatoria + " Cierra en " + nroDias + " d&iacute;a(s)</p></div> </div> " }
                else if (nroDias >= 3) { contador++; textoFechas += " <div class='small-notification yellow-notification'><p class='parrafo-alertas'>" + results.rows.item(i).nombre_convocatoria + " Cierra en " + nroDias + " d&iacute;a(s)</p></div> </div> " }
                else if (nroDias >= 1) { contador++; textoFechas += " <div class='small-notification red-notification'><p class='parrafo-alertas'>" + results.rows.item(i).nombre_convocatoria + " Cierra en " + nroDias + " d&iacute;a(s) </p></div> </div> " }
            }

        }
        if (contador == 0) { textoFechas = "<div class='small-notification green-notification'><p class='parrafo-alertas'>No existen ofertas de empleo favoritas pr&oacute;ximas a cerrar</p></div> </div>"; }
        localStorage.setItem("textoAlertas", textoFechas);
    }
    else {
        textoFechas = "<div class='small-notification green-notification'><p class='parrafo-alertas'>No existen ofertas de empleo marcadas para recordar</p></div> </div>";
        localStorage.setItem("textoAlertas", textoFechas);
    }

    cargarAlertas();

}

//Obtiene la fecha actual en formato dd-mm-yyyy    
function obtenerFechaActual() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '-' + mm + '-' + yyyy;
    return today
}

//N�mero de d�as entre dos fechas
function getNumeroDeNits(fecha1, fecha2) {


    var d1 = fecha1.split("-");
    var dat1 = new Date(d1[2], parseFloat(d1[1]) - 1, parseFloat(d1[0]));
    var d2 = fecha2.split("-");
    var dat2 = new Date(d2[2], parseFloat(d2[1]) - 1, parseFloat(d2[0]));

    var fin = dat2.getTime() - dat1.getTime();
    var dias = Math.floor(fin / (1000 * 60 * 60 * 24))

    return dias;
}

//Consultar si estan aprobados los terminos y condiciones
function consultarTerminos() {
    var db = window.openDatabase("bd_cnsc", "1.0", "Consultar terminos", 200000);
    db.transaction(ConsultarTerminosAceptados, errorOperacionTerminos, efectuadaOperacionTerminos);
}

function ConsultarTerminosAceptados(tx) {
    tx.executeSql('SELECT * FROM TerminosCondiciones WHERE Aprobados = 1', [], crearVariableTerminos, function (error) {
        console.log("consulta termino fin error: " + error)
    });
}

function crearVariableTerminos(tx, results) {
    var len = results.rows.length;
    localStorage.setItem("terminos_aprobados", 0);
    if (len > 0) {
        localStorage.setItem("terminos_aprobados", 1);
        redireccionarIndexenTerminos();
    }
    else {
        localStorage.setItem("terminos_aprobados", 0);
        //window.location.href = "terminos.html";
    }
}

// Transaction error callback
//
function errorOperacionTerminos(err) {
    console.log(err);
    alert("Error procesando la consulta de la ultima cantidad: " + err);
}
// Transaction success callback
//
function efectuadaOperacionTerminos() {
    console.log("Informaci�n Almacenada!");
}

function AceptarTerminosGuardar() {
    var db = window.openDatabase("bd_cnsc", "1.0", "Guardar aprobacion", 200000);
    db.transaction(GuardarTerminosBD, errorOperacionTerminos, efectuadaOperacionTerminos);
}

function GuardarTerminosBD(tx) {
    tx.executeSql("INSERT INTO TerminosCondiciones VALUES (1)", [], redireccionarTerminos, function (error) {
        console.log("consulta termino fin error: " + error)
    });
}

function redireccionarTerminos(tx, results) {
    redireccionarIndexenTerminos();
}


function consultarTerminosIndex() {
    var db = window.openDatabase("bd_cnsc", "1.0", "Consultar terminos", 200000);
    db.transaction(ConsultarTerminosAceptadosIndex, errorOperacionTerminos, efectuadaOperacionTerminos);
}

function ConsultarTerminosAceptadosIndex(tx) {
    tx.executeSql('SELECT * FROM TerminosCondiciones WHERE Aprobados = 1', [], crearVariableTerminosIndex, function (error) {
        console.log("consulta termino fin error: " + error)
    });
}

function crearVariableTerminosIndex(tx, results) {
    var len = results.rows.length;
    localStorage.setItem("terminos_aprobados", 0);
    if (len > 0) {
        localStorage.setItem("terminos_aprobados", 1);
        //window.location.href = "Index.html";
    }
    else {
        localStorage.setItem("terminos_aprobados", 0);
        redireccionarTerminosNoAprobados();
    }
}

function GuardarAgregarFavoritos() {
    var db = window.openDatabase("bd_cnsc", "1.0", "Guardar aprobacion", 200000);
    db.transaction(GuardarAgregarFavoritosBD, errorOperacionTerminos, efectuadaOperacionTerminos);
}

function GuardarAgregarFavoritosBD(tx) {
    var actual = obtenerFechaActual();
    tx.executeSql("INSERT INTO favoritos VALUES ('" + actual + "', '" + localStorage.getItem("id_convocatoria") + "', '" + localStorage.getItem("nombre_convocatoria") + "', '" + window.empleo_conv + "', '" + cambiarFormato(localStorage.getItem("fecha_convocatoria")) + "', '" + window.numero_dias + "')", [], GuardarAgregarFavoritosCorrecto, function (error) {
        console.log("Agregar a favoritos error: " + error)
    });
}

function GuardarAgregarFavoritosCorrecto(tx, results) {
    $('#' + window.div_add_favoritos).removeClass("imagen_favoritos");
    $('#' + window.div_add_favoritos).addClass("imagen_favoritos_full");
}

function cambiarFormato(fecha) {
    var fechaSplit = fecha.split('-');
    var nuevaFecha = fechaSplit[2] + '-' + fechaSplit[1] + '-' + fechaSplit[0]
    return nuevaFecha;
}